//
//  Classmate.swift
//  Lesson2
//
//  Created by Wuttipat Ngoenpechploy on 2017-07-05.
//  Copyright © 2017 Wuttipat Ngoenpechploy. All rights reserved.
//

import Foundation

class Classmate{
    let name :String!
    let country:String!
    init(name:String , country : String){
        self.name = name
        self.country = country
    }
}
