//
//  MainTableViewController.swift
//  Lesson2
//
//  Created by Wuttipat Ngoenpechploy on 2017-07-05.
//  Copyright © 2017 Wuttipat Ngoenpechploy. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {
    var categories = [String]()
    func loadData(){
        categories.append("Computers")
        categories.append("Tablets")
        categories.append("Washing machines")
        categories.append("Games")
        categories.append("Games")
        categories.append("Headphones")
    }
    var classmates : [Classmate]!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
        classmates = [Classmate]()
        classmates.append(Classmate(name:"Haem" ,country:"fdsf"))
        classmates.append(Classmate(name:"J" ,country:"nm"))
        classmates.append(Classmate(name:"K" ,country:"dfdg"))
        
        tableView.register(UINib(nibName:"TableViewCellTwo" , bundle : nil), forCellReuseIdentifier: "categoryCells")
        tableView.register(UINib(nibName: "TableViewCell3", bundle: nil ), forCellReuseIdentifier: "categoryCells2")
        //tableView.register(UINib(), forCellReuseIdentifier: "asdf")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0{
            return categories.count
        }
        if section == 1{
             return classmates.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCells", for: indexPath) as! TableViewCellTwo
            // Configure the cell...
            //let cell = UITableViewCell()
            let category = categories[indexPath.row]
            cell.myFirstLabel?.text = category
            
            return cell
        }
       
        if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCells2", for: indexPath) as! TableViewCell3
            // Configure the cell...
            //let cell = UITableViewCell()
            let classmate = classmates[indexPath.row]
            cell.setupCell(withClassmate: classmate)
            //cell.mylabel2?.text = classmates[indexPath.row]

            
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
        return 44
        }
        return 80
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
