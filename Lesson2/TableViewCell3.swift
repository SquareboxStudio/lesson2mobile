//
//  TableViewCell3.swift
//  Lesson2
//
//  Created by Wuttipat Ngoenpechploy on 2017-07-05.
//  Copyright © 2017 Wuttipat Ngoenpechploy. All rights reserved.
//

import UIKit

class TableViewCell3: UITableViewCell {

    @IBOutlet weak var mylabel2: UILabel!
    
    @IBOutlet weak var mylabel3: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(withClassmate classmate: Classmate){
        mylabel2.text = classmate.name
        mylabel3.text = classmate.country
    }
    
}
